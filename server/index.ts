import * as stencil from '@seobryn/design-system/hydrate'
import express from 'express'
import next from 'next'
import { ParsedUrlQuery } from 'querystring'

const PORT = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare().then(() => {
  const server = express()

  server.get('/_next/*', (req, res) => {
    return handle(req, res)
  })

  server.all('*', async (req, res) => {
    const html = await app.renderToHTML(
      req,
      res,
      req.path,
      req.query as ParsedUrlQuery
    )
    const renderHtml = await stencil.renderToString(html)
    return res.send(renderHtml.html)
  })

  server.listen(PORT, () => {
    console.log(`Next ready on port ${PORT}`)
  })
})
