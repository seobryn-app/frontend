declare namespace JSX {
    interface IntrinsicElements {
        "seo-button": any
        "seo-card": any
        "seo-container": any
        "seo-divider": any
        "seo-header": any
        "seo-img": any
        "seo-input": any
        "seo-logo": any
        "seo-menu": any
        "seo-typography": any
    }
  }