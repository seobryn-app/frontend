import type { NextPage } from 'next'
import React from 'react'

const Home: NextPage = () => {

  return (
    <seo-container>
      <seo-typography variant="h1">Hello World Testing</seo-typography>
    </seo-container>
  )
}

export default Home
